\documentclass[10pt,a4paper,fleqn]{scrartcl}
%\usepackage[a4paper]{geometry}

\usepackage[utf8]{inputenc}
\usepackage[francais]{babel}
\usepackage[T1]{fontenc}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{epigraph}
\usepackage{tgcursor}
\usepackage{graphicx}
\usepackage{amsthm}
\usepackage{ stmaryrd } %pour Mapsto
\usepackage{mathpartir}

\usepackage{ifthen}
\usepackage{textcomp}

\usepackage{hyperref}
\usepackage{csquotes}


\usepackage[style=authortitle,hyperref=true,backend=bibtex]{biblatex}

\bibliography{biblio}

\renewcommand{\FrenchLabelItem}{\textperiodcentered}

\usepackage{cleveref}

\renewcommand{\sectfont}{\rmfamily\bfseries}

\newcommand{\ttsf}[1]{\text{\textsf{#1}}}

\newcommand{\utt}[1]{\underline{\texttt{#1}}}
\newcommand{\tutt}[1]{\text{\utt{#1}}}
\newcommand{\ttt}[1]{\text{\texttt{#1}}}

\newcommand{\props}{\mathcal{P}(\mathbb{I}_\Omega)}

\newcommand{\deduc}[3][\rho]{#1 \vdash \text{#2} \Mapsto \text{#3}}

\newcommand{\abs}{\mathcal{L}}

\newcommand{\setsep}{\ |\ }

\newcommand{\fwd}[1][A]{\overrightarrow{\textit{#1}}}
\newcommand{\bwd}[1][A]{\overleftarrow{\textit{#1}}}


\newcommand{\corrgal}[4]{{#1 
\begin{smallmatrix}
\scriptstyle\leftarrow #4\\
\scriptstyle#2 \rightarrow
\end{smallmatrix} 
#3}}


\title{Interprétation abstraite}
\author{Arthur Carcano}


\newtheoremstyle{persoth}% name of the style to be used
  {}% measure of space to leave above the theorem. E.g.: 3pt
  {}% measure of space to leave below the theorem. E.g.: 3pt
  {}% name of font to use in the body of the theorem
  {}% measure of space to indent
  {\bfseries}% name of head font
  {}% punctuation between head and body
  {\newline}% space after theorem head; " " = normal interword space
  {\thmname{#1}\thmnumber{ #2}\ifthenelse{\equal{#3}{}}{}{ : #3 \addcontentsline{toc}{paragraph}{#1 #2 : #3}}}% Manually specify head
  

\newtheoremstyle{persocoro}% name of the style to be used
  {}% measure of space to leave above the theorem. E.g.: 3pt
  {}% measure of space to leave below the theorem. E.g.: 3pt
  {}% name of font to use in the body of the theorem
  {}% measure of space to indent
  {\bfseries}% name of head font
  {}% punctuation between head and body
  { }% space after theorem head; " " = normal interword space
  {}% Manually specify head
  
\theoremstyle{persocoro}
\newtheorem{coro}{Corollaire}

\theoremstyle{persoth}
\newtheorem{theo}{Théorème}

\newenvironment{preuve}{\noindent\textsc{Démonstration}\par}{$\square$\newline\par}



\begin{document}

\maketitle

\begin{epigraphs}
\qitem{Beware of bugs in the above code; I have only proved it correct, not tried it.}{Donald Knuth}
\end{epigraphs}


\begin{abstract}
Ce T.I.P.E. porte sur l'\textbf{interprétation abstraite}. 
Nous y expliquerons ce qu'est l'interprétation abstraite, introduirons certains des mécanismes utilisés dans l'industrie par l'analyseur \textsc{Astrée} et implémenterons un outil d'analyse statique sur \textsc{Calc}, un langage jouet de calcul sur les entiers à des fins d'illustration.
\end{abstract}


En un demi-siècle l'informatique s'est imposé au cœur de nos industries, faisant de la correction des milliers de lignes de codes écrites chaque jour dans le monde en enjeu majeur, pouvant se chiffrer en centaines de millions d'euros\footnote{L'explosion du premier lanceur Ariane 5, due à un dépassement d'entier, représente un perte de \$370 millions.}. 
Si un humain est capable de prouver la correction d'un code, le volume de travail que représente la rédaction et la vérification par ses pairs d'une telle preuve rend impossible la mise en place de telles pratiques et nous impose de les déléguer à des machines. 

Un typage fort comme celui d'OCaml ou Haskell permet d'éviter certaines erreurs mais ne garantit pas leur abscence : un programme qui compile ne vas malheureusement pas nécessairement marcher : on parle alors d'\textit{erreur à l'exécution}. 
 Des systèmes récents comme Coq permettent d'inclure des preuves dans le code, qui sont vérifiées à la compilation, mais celles-ci ne sont que des preuves de respect d'une spécification, et rien ne garantit que celle-ci sera exhaustive !

L'\textbf{interprétation abstraite} vise à faire vérifier par une machine et de manière exhaustive que l'exécution d'un programme se déroulera sans erreurs. Cette exhaustivité a bien sûr un prix : si il est certain que toutes les erreurs possibles seront débusquées, l'analyseur statique peut parfois donner lieu à de faux positifs.


\section{Présentation informelle}
En interprétation abstraite, nous considérerons les erreurs d'exécution comme des valeurs à part entière : une fonction division par exemple peut nous renvoyer 0, 1.3 ou \textsf{erreurdivzéro}. La division par zéro n'entraine pas un arrêt brusque du programme comme le ferait une exception \textsc{OCaml} mais retourne simplement une valeur qui "remontera" et le résultat final vaudra par exemple \textsf{erreur}.  Nous formaliserons bientôt ce concept en donnant une \textbf{sémantique} à notre langage, c'est à dire en faisant correspondre à la réalité physique (ce qui se passe en machine) un modèle mathématique sur lequel nous résonnerons.

L'exécution d'un programme s'apparente, modulo les interactions avec l'utilisateur, à un système dynamique : des valeurs machine évoluent selon des lois déterministes et suivent des orbites, que nous nommerons plutôt ici « trace ». Il est bien sûr impossible prévoir toutes les traces possible d'exécution d'un programme en fonction des valeurs d'entrée, puisque celles-ci sont infinies. Plutôt que de chercher à connaître toutes les traces possibles, l'interprétation abstraite va donc étudier une enveloppe qui les contient et vérifier que cette enveloppe n'a pas d'intersection avec une "zone interdite".
Sur la figure ci-dessous, qui considère que seules deux traces sont possibles on remarquera en A un faux-positif : la trace la plus haute ne passe pas par la zone interdite (en rouge) mais l'enveloppe étant trop simpliste, il y a intersection. En B par contre, l'erreur est justifiée.

\begin{figure}[h]
\centering
\def\svgwidth{\columnwidth}
\includegraphics[scale=0.5]{schemaAI.pdf}
\end{figure}

\section{Calc et sa sémantique}
On donne ici la sémantique d'un langage d'arithmétique sur les entiers que l'on baptisera \textsc{Calc}, inspiré de celui utilisé dans \cite{cousot}. 

On définit les valeurs suivantes :
\newline

\begin{itemize}
\item \textsf{min\_int} $\in \mathbb{Z}$ (respectivement \textsf{max\_int}) est le plus petit (respectivement le plus grand) entier représentable en machine. En supposant que le langage stock les entiers sur 32 bits on a donc $\ttsf{min\_int} + 1 - \ttsf{max\_int} = 2^{32}=4\,294\,967\,296$.
\item  $\mathbb{I} = \left[ \ttsf{min\_int},\ttsf{max\_int} \right]$ est l'ensemble des entiers représentables en machine.
\item $\Omega_i$ représentera une valeur non initialisée, c'est à dire une variable à laquelle on a pas encore donné de valeur.
\item $\Omega_a$ représentera une erreur d'arithmétique, typiquement une division par zéro mais aussi un dépassement d'entier. (VRAIMENT ?)
\item $\mathbb{E} = \left\{\Omega_i, \Omega_a\right\}$
\item $\mathbb{I}_\Omega = \mathbb{I} \cup \mathbb{E}$
\item les symboles mathématiques +,-,/ et * représentent à la fois la fonction \textsc{Calc} et son pendant mathématique\newline
\end{itemize}


On notera en \textsf{sans serif} les éléments d'interprétation sémantique, en \textit{italique} les éléments du méta-langage mathématique et en \texttt{chasse fixe} les portions de code. Pour associer à une portion de code \texttt{blbl} son interprétation sémantique, on la soulignera comme ceci : \utt{blbl}. On a alors :
\label{semantique}

\begin{itemize}
\item Si \texttt{d}$_1$\dots\texttt{d}$_k$ est une suite de chiffres, elle donnera une erreur si le nombre qu'elle représente est trop gros et un entier de $\mathbb{I}$ sinon :
	\begin{itemize}
	\item $\utt {d} = \textsf{d}$ où \texttt{d} est un chiffre entre 0 et 9
	\item  \underline{\texttt{d}$_1$\dots\texttt{d}$_k$} = 10\underline{\texttt{d}$_1$\dots\texttt{d}$_{k-1}$} + \textsf{d} si \underline{\texttt{d}$_1$\dots\texttt{d}$_{k-1}$} $\in \mathbb{I}$ et 10\underline{\texttt{d}$_1$\dots\texttt{d}$_{k-1}$} + \textsf{d} $\leq$ \textsf{max\_int}. 
	\item  \underline{\texttt{d}$_1$\dots\texttt{d}$_k$} = $\Omega_a$ si \underline{\texttt{d}$_1$\dots\texttt{d}$_{k-1}$} $\not\in \mathbb{I}$ ou 10\underline{\texttt{d}$_1$\dots\texttt{d}$_{k-1}$} + \textsf{d} > \textsf{max\_int}. 
	\end{itemize}
	
\item Opérateur unaire -  : 
	\begin{itemize}
	\item pour \textsf{n} $\in \mathbb{I}$, \utt{-}\textsf{n} = \textsf{-n} si \textsf{-n} $\in \mathbb{I}$, $\Omega_a$ sinon.
	\item \utt{-}$\Omega_a$ = $\Omega_a$ (on propage l'erreur)
	\item \utt{-}$\Omega_i$ = $\Omega_i$ 
	\end{itemize}
	
\item Opérateurs binaires autres que la division : on donne la sémantique pour +, les autres s'écrivent de manière similaire
	\begin{itemize}
	\item pour \textsf{n},\textsf{m}  $\in \mathbb{I}$, \textsf{n} \utt{+} \textsf{m} = \textsf{n+m} si \textsf{n+m} $\in \mathbb{I}$, $\Omega_a$ sinon.
	\item \textsf{n} \utt{+} $\Omega_a$ = $\Omega_a$ (on propage l'erreur)
	\item \textsf{n} \utt{+} $\Omega_i$ = $\Omega_i$ 
	\item $\Omega_a$ \utt{+} \textsf{n} = $\Omega_a$ 
	\item $\Omega_i$ \utt{+} \textsf{n} = $\Omega_i$ 
	\end{itemize}
	
\item Division
	\begin{itemize}
	\item pour \textsf{n},\textsf{m}  $\in \mathbb{I}$,\textsf{m} $\neq 0$, \textsf{n} \utt{/} \textsf{m} = \textsf{n/m} si \textsf{n/m} $\in \mathbb{I}$, $\Omega_a$ sinon.
	\item \textsf{n} \utt{/} $\Omega_a$ = $\Omega_a$ (on propage l'erreur)
	\item \textsf{n} \utt{/} $\Omega_i$ = $\Omega_i$ 
	\item $\Omega_a$ \utt{/} \textsf{n} = $\Omega_a$ 
	\item $\Omega_i$ \utt{/} \textsf{n} = $\Omega_i$ 
	\end{itemize}
\end{itemize}

\section{Propriétés et correspondances de Galois}
On souhaite maintenant étudier des propriétés de sous ensembles de $\mathbb{I}_\Omega$. Une propriété $P$ est elle même un sous ensemble de $\mathbb{I}$ et dire qu'un élément $x \in \mathbb{I}_\Omega$ satisfait à $P$, c'est dire que $x \in P$. De même dire que $A \subseteq \mathbb{I}_\Omega$ satisfait à $P$ c'est dire que $A \subseteq P$. On notera $\mathcal{P}(\mathbb{I}_\Omega)$ l'ensemble des propriétés. 

Seul problème, sur une machine 32 bits, cela nous fait $2^{2^{32}}$ propriétés différentes, soit environ $10^{1292913986}$ (plus d'un milliards de chiffres). Devant de telles proportions, nous devons donc sélectionner, mais d'une manière réfléchie.
\subsection{L'ensemble des propriétés : un treillis complet}
Remarquons rapidement que $\left(\mathcal{P}(\mathbb{I}_\Omega),\subseteq,\emptyset,\mathbb{I}_\Omega,\cup,\cap\right)$ est un treillis complet\footcite[Voir par exemple :][]{coursfurst}, c'est à dire un ensemble partiellement ordonné dont toute partie admet une borne supérieure, donnée par l'opérateur $\cup$ et une borne supérieure ($\cap$). Du point de vue "logique", $\subseteq$ correspond à une implication, $P_1 \cup \dots \cup P_k$ à la plus forte propriété impliquée par $P_1\dots P_k$, c'est le ou logique et $P_1 \cap \dots \cap P_k$ à la plus faible propriété qui implique $P_1\dots P_k$, c'est le et logique.

\subsection{Abstraction et concrétisation}
\label{definitionalphagamma}
On a dit précédemment qu'on allait "sélectionner" des propositions. Cela n'est pas tout à fait vrai. On va en réalité considérer un ensemble tout à fait nouveau que l'on reliera à l'ensemble des propriétés par une correspondance de Galois isotone.  Ce nouvel ensemble sera noté $\mathcal{L}$, et à chaque élément $p \in \mathcal{L}$ on associera $\gamma(p) \in \mathcal{P}(\mathbb{I}_\Omega)$. $\gamma$ sera appelé \textbf{concrétisation}. $\gamma(p)$ sera la propriété réelle, concrète qui correspond à une propriété abstraite. Si on avait pris pour $\mathcal{L}$ une partie de $\mathcal{P}(\mathbb{I}_\Omega)$, $\gamma$ serait l'identité restreinte à $\mathcal{L}$. On va de plus choisir $\abs$ comme étant également un treillis complet : $\left(\abs,\sqsubseteq,\bot,\top,\sqcup,\sqcap\right)$

Si à toute proposition abstraite on fait ainsi correspondre une propriété, l'inverse n'est pas vrai ($\gamma$ n'est pas surjective en général puisqu'on a pris $\abs$ plus petit que $\props$). Or, on veut pouvoir modélisé toute propriété par une proposition abstraite. Raisonnons qualitativement : on a vu qu'on acceptait des faux positifs mais qu'on voulait que notre modélisation soit exhaustive, c'est à dire que toutes les erreurs possibles soient détectées. On va donc vouloir faire correspondre à $P \in \props$ une $p \in \abs$ telle que $P \subseteq \gamma(p)$. Seulement, on ne veut pas non plus trop sur-approximer, on voudrait donc la plus faible des $p$ qui convient, si cela a du sens.

\begin{theo}
Soit $P \in \props$.\\
\label{defuumorphisme}On suppose que $\gamma$ est un $\sqcap$-$\cap$-morphisme, c'est à dire que pour toute partie $A$ de $\abs$, on a $\cap \left\{\gamma(x), x\in A \right\} = \gamma\left( \sqcap A\right)$.\\
\textbf{Alors} $\left\{p \in \abs \setsep P \subseteq \gamma(p) \right\}$ admet un minimum : $\sqcap \left\{p \in \abs \setsep P \subseteq \gamma(p) \right\}$.
\end{theo}

\begin{preuve}
Fixons $P \in \props$.

On a $P \subseteq \gamma\left( \sqcap \left\{p \in \abs \setsep P \subseteq \gamma(p) \right\} \right)$. 

Par hypothèse, $\gamma\left( \sqcap \left\{p \in \abs \setsep P \subseteq \gamma(p) \right\} \right) = \cap\left\{\gamma(p) \setsep p \in \abs, P \subseteq \gamma(p) \right\}$, d'où par définition de la borne inférieure : $P \subseteq \gamma\left( \sqcap \left\{p \in \abs \setsep P \subseteq \gamma(p) \right\} \right)$. La borne inférieure est bien un minimum.
\end{preuve}

Ainsi, si on impose que $\gamma$ soit un $\sqcap$-$\cap$-morphisme, on peut définir l'\textbf{abstraction} : $\alpha : P \mapsto \sqcap \left\{p \in \abs \setsep P \subseteq \gamma(p) \right\}$. On fera cette hypothèse par la suite, et on généralise ce raisonnement au \cref{unseuladjoint} (p. \pageref{unseuladjoint}).

\subsection{Correspondances de Galois isotones}
\subsubsection{Définition}
\label{defcorr}
Formellement, une correspondance de galois isotone\footnote{Il existe des correspondances de Galois antitones, que nous n'utiliserons pas ici. On omettra donc parfois le "isotone".} entre deux ensembles partiellement ordonné $(A,\leq_A)$ et $(B,\leq_B)$ est un couple de fonctions\footnote{On notera $A \rightarrow B$ ou $B^A$ l'ensemble des fonctions de $A$ dans $B$.} $(\alpha,\beta) \in B^A \times A^B $ tel que pour tout $a \in A$, $b \in B$, $\alpha(a) \leq_B b \Longleftrightarrow a \leq_A \beta(b)$. On la notera $\corrgal{A}{\alpha}{B}{\beta}$. $\alpha$ est appelé l'adjoint inférieur et $\beta$ l'adjoint supérieur. On remarquera qu'ils n'ont pas des rôles symétriques.

\begin{theo}[Caractérisation d'une correspondance de Galois]
\label{equivgal}
Soit $(A,\leq_A)$ et $(B,\leq_B)$ deux ensemble partiellement ordonnés et $(\alpha,\beta) \in B^A \times A^B $. $\corrgal{A}{\alpha}{B}{\beta}$ est une correspondance de Galois si et seulement si :
\begin{enumerate}
\item $\alpha$ et $\beta$ sont croissantes 
\item pour tout $a \in A$, $a \leq_A \beta \circ \alpha(a)$ ($\beta \circ \alpha$ est expansive)
\item pour tout $b \in B$, $\alpha \circ \beta(b)\leq_B b $ ($\alpha \circ \beta$ est non expansive)
\end{enumerate}
\end{theo}
\begin{preuve}
On considère $A$, $B$, $\alpha$ et $\beta$ fixés et on procède par double implication :


\fbox{$\Longrightarrow$} On suppose que $\corrgal{A}{\alpha}{B}{\beta}$ est une correspondance de Galois et on commence par montrer 2 et 3. 

Soit $a \in A$  On a $\alpha(a) \leq_B \alpha(a)$ donc $a \leq_A \beta \circ \alpha (a)$. De même, pour $b \in B$, $\beta(b) \leq_A \beta(b)$ nous donne $\alpha \circ \beta (b) \leq_B b$. 

Montrons maintenant la croissance de $\alpha$ (celle de $\beta$ s'obtient de manière similaire). Soit $a_1$, $a_2$ dans $A$, tels que $a_1 \leq_A a_2$. Alors $a_1 \leq_A a_2 \leq_A \beta \circ \alpha (a_2)$ d'après 2, donc $a_1 \leq_A \beta \circ \alpha (a_2)$, qui se ré-écrit : $\alpha(a_1) \leq_B \alpha(a_2)$.


\fbox{$\Longleftarrow$} On suppose maintenant que 1,2 et 3 sont vraies et on montre que $\corrgal{A}{\alpha}{B}{\beta}$ est une correspondance de Galois. 

Soit $a \in A$, $b \in B$. 

On suppose $\alpha(a) \leq_B b$ donc par croissance de $\beta$, $\beta \circ \alpha (a) \leq_A \beta(b)$. Or d'après 2, $\beta \circ \alpha$ est expansive d'où $a \leq_A \beta(b)$.

La réciproque se traite de manière similaire.
\end{preuve}

\subsubsection{Des théorèmes utiles}

On donne maintenant quelques propriétés des connections de Galois. Celles-ci nous serviront \cref{application}, afin de formaliser notre interprétation abstraite.

\begin{theo}[Sur un treillis, une correspondance de Galois est entièrement définie par un de ses adjoints.]
\label{unseuladjoint}
Soit $(A,\leq_A,\bot_A,\top_A,\cup_A,\cap_A)$ et $(B,\leq_B,\bot_B,\top_B,\cup_B,\cap_B)$ deux treillis complets.\\
\textbf{Alors} tout $\cup_A$-$\cup_B$-morphisme $\alpha$ (resp. $\cap_B$-$\cap_A$-morphisme $\beta$) est l'adjoint inférieur (resp. supérieur) d'une unique correspondance de Galois entre $A$ et $B$ donné par $\corrgal{A}{\alpha}{B}{\beta}$ où $\beta(b) = \cup_A\left\{a\in A\setsep \alpha(a) \leq_B b \right\}$ (resp. $\alpha(a)=\cap_B\left\{b\in B\setsep a \leq_B \beta(b) \right\}$)
\end{theo}
\begin{preuve}
Soit $(A,\leq_A,\bot_A,\top_A,\cup_A,\cap_A)$ et $(B,\leq_B,\bot_B,\top_B,\cup_B,\cap_B)$ deux treillis complets et $\alpha \in B^A$ un $\cup_A$-$\cup_B$-morphisme. On remarque tout de suite que $\alpha$ est une application croissante. En effet, pour tout $a_1$, $a_2 \in A$ tels que $a_1 \leq_A a_2$, on a :

\[
\begin{aligned}
\max(\alpha(a_1),\alpha(a_2))&= \alpha(a_1) \cup_B \alpha(a_2) \\
							&= \alpha(a_1\cup_A a_2)\\
							&=\alpha(a_2) 
\end{aligned}
\]

donc $\alpha(a_1) \leq_B \alpha(a_2)$.

\textbf{Analyse :} On suppose qu'il existe $\beta$ tel que $\corrgal{A}{\alpha}{B}{\beta}$ soit une correspondance de Galois.

 Soit $b \in B$ fixé. 
 
Soit $a \in A$ tel que $\alpha(a) \leq_B b$, on a $a \leq_A \beta(b)$ donc $\cup_A\left\{a\in A\setsep \alpha(a) \leq_B b \right\} \leq_A \beta(b)$. 
 
 Or $\alpha \circ \beta (b) \leq_B b$ donc $\beta(b) \leq_A \cup_A\left\{a\in A\setsep \alpha(a) \leq_B b \right\}$. 

Ainsi $\beta(b) = \cup_A\left\{a\in A\setsep \alpha(a) \leq_B b \right\}$

\textbf{Synthèse :} On vérifie que si l'on pose : $\beta : b \mapsto \cup_A\left\{a\setsep \alpha(a) \leq_B b \right\}$, $\corrgal{A}{\alpha}{B}{\beta}$ est bien une correspondance de Galois, grâce au \cref{equivgal}. 

Soit $b_1$, $b_2 \in B$, $b_1 \leq_B b_2$. 

On a $\left\{a \in A\setsep \alpha(a) \leq_B b_1 \right\} \subseteq \left\{a \in A\setsep \alpha(a) \leq_B b_2 \right\}$ 

donc $\cup_A\left\{a \in A\setsep \alpha(a) \leq_B b_1 \right\} \leq_A \cup_A\left\{a \in A\setsep \alpha(a) \leq_B b_2 \right\}$, 

ie. $\beta(b_1) \leq_A \beta(b_2)$. 

$\beta$ est donc bien croissante.

Soit $b \in B$ :
\[
\begin{aligned}
\alpha \circ \beta (b) &= \alpha \left(\cup_A\left\{a\in A\setsep \alpha(a) \leq_B b \right\}\right)\\
						&=\cup_B \left\{\alpha(a) \setsep a \in A, \alpha(a) \leq_B b \right\} \text{ (Car $\alpha$ est un $\cup_A$-$\cup_B$-morphisme)}
\end{aligned}
\]
donc $\alpha \circ \beta (b) \leq_B b$.

Soit $a \in A$. $a$ appartient à $\left\{x \in A \setsep \alpha(x) \leq_B \alpha(a)\right\}$ donc $a \leq_A \cup_A \left\{x \in A \setsep \alpha(x) \leq_B \alpha(a)\right\}$ ie. $a \leq_A \beta \circ \alpha(a)$.

On a bien vérifié les trois points du \cref{equivgal}.

On procéderait de même si on avait l'adjoint supérieur au départ.
\end{preuve}

On en déduit immédiatement

\begin{coro}
Pour le $\gamma$ et le $\alpha$ défini en \cref{definitionalphagamma}, $\corrgal{\props}{\alpha}{\abs}{\gamma}$ est une correspondance de Galois isotone.
\end{coro}

\begin{theo}[Une réciproque du \cref{unseuladjoint}]
\begin{samepage}
Soit $(A,\leq_A,\bot_A,\top_A,\cup_A,\cap_A)$ et $(B,\leq_B,\bot_B,\top_B,\cup_B,\cap_B)$ deux treillis complets et $\corrgal{A}{\alpha}{B}{\beta}$ une correspondance de Galois. \\
\textbf{Alors} :
\begin{enumerate}
\item $\alpha$ est un $\cup_A$-$\cup_B$-morphisme
\item $\beta$ est un $\cap_B$-$\cap_A$-morphisme
\end{enumerate}
\end{samepage}
\end{theo}

\begin{preuve}
On montre le 1, le 2 se faisant de manière similaire.

Soit $\Delta$ une partie de $A$. 

\fbox{$\leq_B$} Pour tout $x \in \Delta$ on a $x \leq_A \cup_A \Delta$ donc $\alpha(x) \leq_B \alpha(\cup_A \Delta)$ (croissance de $\alpha$) donc $\cup_B \alpha(\Delta) \leq_B \alpha(\cup_A \Delta)$.

\fbox{$\geq_B$} Pour tout $x \in \Delta$ on a $\alpha (x) \leq_B \cup_B \alpha(\Delta)$, donc $x \leq_A \beta(\cup_B \alpha(\Delta))$ (d'après la définition d'une correspondance de Galois - cf. \cref{defcorr}). Ceci étant vrai pour tout $x \in \Delta$,  on a $\cup_A \Delta \leq_A \beta(\cup_B \alpha(\Delta))$ donc $\alpha(\cup_A \Delta) \leq_B \cup_B \alpha(\Delta)$

Ainsi $\cup_B \alpha(\Delta) = \alpha(\cup_A \Delta)$
\end{preuve}

\begin{theo}[Réunion des applications]
\label{reuapp}
On se permet ici de ne plus différencier les relations d'ordres en fonction de l'ensemble où elles ont valeur. Soit $(A, \leq)$ et $(B, \leq)$ deux ensembles partiellement ordonnés. On munit tout les ensembles de parties de l'ordre $\subseteq$ et ceux de fonctions de l'ordre point à point. On définit :
\[
\begin{array}{r@{\ :\ }c c l}
\alpha & \mathcal{P}\left(B^A\right) & \longrightarrow & {\mathcal{P}(B)}^A\\
 	   &     X            & \longmapsto     & (a \mapsto \left\{x(a) \setsep x \in X \right\})\vspace{1em}\\

\beta &  {\mathcal{P}(B)}^A & \longrightarrow  &\mathcal{P}\left(B^A\right)\\
 	   &     r           & \longmapsto     &  \left\{p \in B^A \setsep \forall a \in A, p(a) \in r(a)\right\})
\end{array}
\]

\textbf{Alors} $\corrgal{\mathcal{P}\left(B^A\right)}{\alpha}{{\mathcal{P}(B)}^A}{\beta}$ est une correspondance de Galois.
\end{theo}

\begin{preuve}
Soit $X \in \mathcal{P}\left(B^A\right)$ et $r \in {\mathcal{P}(B)}^A$. Alors :
\[
\begin{aligned}
\alpha(X) \leq r &\Longleftrightarrow \forall a \in A, {x(a) \setsep x \in X} \subseteq r(a)\\
				&\Longleftrightarrow \forall a \in A, x \in X, x(a) \in r(a)\\
				&\Longleftrightarrow \forall x \in X, x \in \left\{p \setsep \forall a \in A, x(a) \in r(a) \right\}\\
\alpha(X) \leq r	&\Longleftrightarrow X \leq \beta(r)
\end{aligned}
\]
D'après la définition (\cref{defcorr}), on a bien une correspondance de Galois.
\end{preuve}

\begin{samepage}
\begin{theo}[Lifting des abstractions]
\label{lifting}
Soit $\corrgal{A}{\alpha}{B}{\beta}$, et $\corrgal{X}{\chi}{Y}{\epsilon}$ deux correspondances de Galois. On note $A\rightharpoonup X$ (resp. $B \rightharpoonup Y$) l'ensemble des applications croissantes de A dans X (resp. de B dans Y).
On pose :
\[
\begin{array}{r@{\ :\ }c c l}
\eta & A\rightharpoonup X & \longrightarrow & B \rightharpoonup Y\\
 	   &     f           & \longmapsto     & \chi \circ f \circ \beta \vspace{1em}\\
\theta & B \rightharpoonup Y & \longrightarrow  & A\rightharpoonup X\\
 	   &     g         & \longmapsto     &  \epsilon \circ g \circ \alpha
\end{array}
\]
\textbf{Alors,} $\corrgal{\left(A \rightharpoonup X\right)}{\eta}{\left(B \rightharpoonup Y\right)}{\theta}$ est une correspondance de Galois.
\end{theo}
\end{samepage}
\begin{preuve}
On utilise le \cref{equivgal} :
\begin{enumerate}
\item Soit $f$ et $g$ dans $A \rightharpoonup X$ avec $f \leq g$. Soit $b \in B$.
\[
\begin{array}{r r c l l}
\text{On a}&\beta(b) &=& \beta(b) &\\
\text{donc} & f\circ \beta (b) &\leq& g \circ \beta (b) &\text{ (car $f \leq g$)}\\
\text{donc} &\chi \circ f\circ \beta (b) &\leq & \chi \circ g \circ \beta (b) &\text{ (car $\chi$ est croissante)}\\
\text{ie.} & \eta(f)(b) &\leq& \eta(g)(b) &\\
\end{array}
\]
Ceci étant vrai pour tout  $b \in B$, on a bien la croissance de $\eta$. On procède de même pour $\theta$.
\item Soit $f \in A \rightharpoonup X$, et $a \in A$. 
\[
\begin{array}{r r c l l}
\text{On a} & a    & \leq & \beta \circ \alpha (a)& \text{ expansivité de $\beta \circ \alpha$}\\
\text{donc} & f(a) & \leq & f \circ \beta \circ \alpha (a)  & \text{ croissance de $f$}\\
\text{mais} & f \circ \beta \circ \alpha (a) &\leq& \epsilon \circ \chi \circ f \circ \beta \circ \alpha (a) & \text{expansivité de $\epsilon \circ \alpha$}\\
\text{donc} &f(a) & \leq & \epsilon \circ \chi \circ f \circ \beta \circ \alpha (a)&\\
\text{ie}   &f(a) & \leq &  \theta \circ \eta (f)(a) &\\
\end{array}
\]
Ceci étant vrai pour tout $a \in A$, on a bien l'expansivité de $\theta \circ \eta$.
\item  On procède comme en 2 pour la non expansivité.
\end{enumerate}

Ainsi, $\corrgal{\left(A \rightharpoonup X\right)}{\eta}{\left(B \rightharpoonup Y\right)}{\theta}$ est bien une correspondance de Galois.
\end{preuve}

\paragraph*{Dernières propriétés}

On donne maintenant d'autres propriétés\footnote{Les trois premières sont citées dans \cite{wiki:corgal}et les deux dernières dans \cite{cousot}.} sans détailler les démonstrations.
Soit $\corrgal{A}{\alpha}{B}{\beta}$ une correspondance de Galois.
\begin{enumerate}
\item $\alpha \circ \beta$ et $\beta \circ \alpha$ sont croissantes (composée de deux fonction croissantes).
\item $\alpha \circ \beta \circ \alpha = \alpha$ et $\beta \circ \alpha \circ \beta = \beta$. (On écrit l'expansivité et la non expansivité et on compose par une application croissante à droite ou gauche).
\item  $\alpha \circ \beta$ et $\beta \circ \alpha$ sont idempotentes (conséquence de la précédente)
\item \label{compoabstr}\textbf{Composition d'abstraction :} Soit $C$ un ensemble partiellement ordonné et $\corrgal{B}{\eta}{C}{\theta}$ une correspondance de Galois. 

Alors $\corrgal{A}{\eta \circ \alpha}{C}{\beta \circ \theta}$ est une correspondance de Galois.
\item \label{absptapt} \textbf{Abstraction point à point :} Soit $X$ un ensemble quelconque.

 Alors $\corrgal{(X \rightarrow A)}{\alpha \circ}{(X \rightarrow B)}{\beta \circ}$ est une correspondance de Galois.
\end{enumerate}

\section{Application : abstraction et collection}
\label{application}
\subsection{Modéliser les états du programme}
On a ici négligé les interactions avec l'utilisateur ainsi que le recours à du hasard stimulé (et de manière général, à tout ce qui n'est pas déterministe). Étudier notre programme c'est donc connaître à chaque instant la valeur de chacune des variables possibles (on utilisera $\Omega_i$ pour une variable jamais utilisée). On note $\mathbb{V}$ l'ensemble des variables possibles et $\mathbb{R} = \mathbb{I}_\Omega^\mathbb{V}$ l'ensemble des \textbf{environnements} (des applications de $\mathbb{V}$ dans $\mathbb{I}_\Omega$). On va définir plusieurs correspondances de Galois successives afin d'arriver à celle que l'on utilisera \textit{in fine}.

On a $\corrgal{\props}{\alpha}{\abs}{\gamma}$ donc, en faisant une abstraction point à point (\cref{absptapt} page \pageref{absptapt}, ici les adjoints ne sont volontairement pas nommés), on a $\corrgal{\left(\mathbb{V} \rightarrow \props\right)}{}{(\mathbb{V} \rightarrow L)}{}$.

En utilisant le \cref{reuapp}, on obtient $\corrgal{\mathcal{P}(\mathbb{R})}{}{(\mathbb{V} \rightarrow \props)}{}$.

Enfin, en les composant on obtient la correspondance : $\corrgal{\mathcal{P}(\mathbb{R})}{\dot \alpha}{(\mathbb{V} \rightarrow \abs)}{\dot \gamma}$, c'est à dire qu'on peut modéliser chaque ensemble d'états possible par une application qui à chaque variable associe une propriété abstraite.

En développant les expressions données dans les théorèmes précédents on trouve : \label{alphaptgammapt}
\[
\begin{array}{r@{\ :\ }c c l}
\dot{\alpha} & \mathcal{P}(\mathbb{R}) & \longrightarrow & \mathbb{V} \rightarrow \abs\\
 	   &    R           & \longmapsto     & x \mapsto \alpha \left\{ \rho(x) \setsep \rho \in R \right\}    \vspace{1em}\\
\dot{\gamma} & \mathbb{V} \rightarrow \abs & \longrightarrow & \mathcal{P}(\mathbb{R}) \\
 	   &     r        & \longmapsto     &  \left\{ \rho \in \mathbb{R} \setsep \forall x \in \mathbb{V}, \rho(x) \in \gamma(r(x)) \right\}
\end{array}
\]

\subsection{Forward/Backward collecting}
On donne rapidement la sémantique de \textsc{Calc} prenant en compte les environnements. (On se référera à \cref{semantique} page \pageref{semantique}) $\deduc{\textit{A}}{\textit{v}}$ signifie que dans l'environement $\rho$, l'expression \textsc{Calc} \textit{A} s'évalue en \textit{v} $\in \mathbb{I}_\Omega$. 


\begin{mathpar}
\label{bigstepsemantique}
\inferrule{\text{la chaîne \texttt{n} est une suite de chiffres}}
		{\deduc{\texttt{n}}
			     {\utt{n}}
	    } \and
\inferrule{\text{la chaîne \texttt{X} représente un élément \utt{X} de $\mathbb{V}$}}
		 {\deduc{\texttt{X}}
		        {$\rho({\tutt{X}})$}
		 } \and
\inferrule{\deduc{\textit{A}}
                  {\textit{v}
                 }
          }
		  {\deduc{\texttt{u} \textit{A}}
		         {\utt{u} \textit{v}}
		  } \and
\inferrule{\deduc{\textit{A}$_1$}
                  {\textit{v}$_1$
                 }\\
	         \deduc{\textit{A}$_2$}
	                {\textit{v}$_2$
	                          }
          }
		  {\deduc{\textit{A}$_1$ \texttt{b} \textit{A}$_2$}
		         {\textit{v}$_1$ \utt{b} \textit{v}$_2$}
		  } \and
\end{mathpar}

On peut désormais raisonner de deux manières sur notre programme (qui se limite dans notre cas à une expression arithmétique) : la première consiste à fixer des \textbf{pré-conditions} et à chercher la plus faible \textbf{post-condition} satisfaite par l'environnement après avoir complètement évalué l'expression. C'est le \textit{forward collecting}\footnote{N'ayant pas trouvé de traduction appropriée, on reprend le terme anglais. Il provient de \cite{cousot}}.

À l'inverse, on peut vouloir que notre environnement satisfasse, après évaluation, à une post-condition, et on cherche la plus faible pré-condition à satisfaire à l'état initial.

On fixe désormais \textit{A} une expression arithmétique (c'est à dire un code \textsc{Calc} syntaxiquement correct.)
\paragraph{Forward Collecting} On note $\fwd$ le \textit{forward collecting} associé à \textit{A}, défini par :
\[
\begin{array}{r@{\ :\ }c c l}
\fwd & \mathcal{P}\left(\mathbb{R}\right) & \longrightarrow & \props\\
 	   &  R  & \longmapsto  & \left\{v \in \mathbb{I}_\Omega \setsep \exists \rho \in R, \deduc{\textit{A}}{\textit{v}} \right\}
\end{array}
\]

$\fwd (R)$ est bien la propriété la plus faible satisfaite après par la valeur de retour de \textit{A} dans un environnement qui satisfait initialement à $R$

\subparagraph*{Remarque :} $\fwd$ est une application croissante.

\paragraph{Backward Collecting} On note $\bwd$ le \textit{backward collecting} associé à \textit{A}, défini par :
\[
\begin{array}{r@{\ :\ }c c c c l}
\bwd & \mathcal{P}\left(\mathbb{R}\right) & \longrightarrow & \props & \longrightarrow & \mathcal{P}\left(\mathbb{R}\right)\\
 	   &  R  & \longmapsto  &P & \longmapsto & \left\{\rho \in R \setsep \exists v \in P \cap \mathbb{I}, \deduc{\textit{A}}{\textit{v}} \right\}
\end{array}
\]

$\bwd (R) (P)$ est la plus faible des propriétés plus fortes que $R$ à satisfaire pour que l'évaluation de \textit{A} se déroule sans erreur (d'où l'intersection avec $\mathbb{I})$ et que la valeur finale satisfasse à $P$.

\subsection{Abstraction du Forward Collecting}
\subsubsection{Lifting}

Si nous avons ainsi formalisé deux types de collection, il nous reste encore à pouvoir les utiliser, et encore une fois les ensembles où ces fonctions ont valeur sont bien trop gros.

Heureusement, nous avons
 $\corrgal{\mathcal{P}(\mathbb{R})}{\dot \alpha}{(\mathbb{V} \rightarrow \abs)}{\dot \gamma}$ et
  $\corrgal{\props}{\alpha}{\abs}{\gamma}$, qui, d'après le 
  \cref{lifting} (page \pageref{lifting}) nous permet d'écrire :

\[
\corrgal{\left(\mathcal{P}\left(\mathbb{R}\right) \rightharpoonup  \props\right)}{\overrightarrow{\alpha}}{\left((\mathbb{V} \rightarrow \abs) \rightharpoonup \abs\right)}{\overrightarrow{\gamma}}
\]

Soit une expression \textit{A} fixée. On cherche à avoir un moyen de réaliser la collection 
dans le domaine "simple" $ \left((\mathbb{V} \rightarrow \abs) \rightharpoonup \abs\right)$. 
Notons $\Xi$ l'hypothétique collecteur dans ce domaine. 
La condition d'exhaustivité s'écrit $\forall p \in \abs, \fwd(\dot{\gamma}(p)) \subseteq \gamma (\Xi(p))$ : « Pour toute propriété abstraite $p$, ce qui va se produire est le résultat de la collection $\fwd$ sur $\dot{\gamma}(p)$, l'environnement qui représente $p$. Si je prends la propriété qui représente la collection que j'ai fait sur le domaine de l'abstrait (qui était plus simple), il faut que j'ai au moins tout les cas qui seraient réellement sortis ».

Or  $\forall p \in \abs, \fwd(\dot{\gamma}(p)) \subseteq \gamma (\Xi(p))$ se ré-écrit :  $\forall p \in \abs, \alpha\circ\fwd(\dot{\gamma}(p)) \subseteq \Xi(p)$ soit d'après le \cref{lifting} $\overrightarrow{\alpha}(\fwd) \leq \Xi$ (ordre point à point).

$\overrightarrow{\alpha}(\fwd)$ est donc le meilleur choix possible pour $\chi$ (celui qui nous donnera la plus forte post-condition). Seul problème, ni $\overrightarrow{\alpha}$ ni $\fwd$ ne sont en pratique calculables par l'ordinateur (\cite[p. 15]{cousot}), on se servira donc de $\overrightarrow{\alpha}(\fwd)$ comme d'une spécification formelle pour expliciter notre collecteur abstrait.

\subsubsection{Explicitons $\Xi$}
On raisonne à $r \in (\mathbb{V} \rightarrow \abs)$ fixé. On notera $\Xi_A$ le collecteur abstrait relatif à une expression \textit{A}.

On commence par expliciter $\overrightarrow{\alpha}(\fwd)$ :
\[
\begin{aligned}
\overrightarrow{\alpha}(\fwd) &= \alpha \circ \fwd \circ \dot{\gamma}\\
							&=  \alpha \circ   \left(R  \mapsto   \left\{v \in \mathbb{I}_\Omega \setsep \exists \rho \in R, \deduc{\textit{A}}{\textit{v}} \right\}\right) \circ \dot{\gamma}\\
							&= r \mapsto \alpha\left( \left\{v \in \mathbb{I}_\Omega \setsep \exists \rho \in \dot{\gamma}(r), \deduc{\textit{A}}{\textit{v}} \right\}\right)
\end{aligned}
\]
On procède ensuite par induction :

\paragraph{\textit{A} = \texttt{n}, ou \texttt{n} représente un nombre}
\[
\begin{aligned}
\overrightarrow{\alpha}(\fwd)(r) 
		&= \alpha\left( \left\{v \in \mathbb{I}_\Omega \setsep \exists \rho \in \dot{\gamma}(r), \deduc{\textit{A}}{\textit{v}} \right\}\right)\\
		&= \alpha\left( \left\{\utt{n} \right\}\right)\\
\end{aligned}
\]

On prend donc \fbox{$\Xi_\ttt{n} : r \mapsto \alpha\left( \left\{\utt{n} \right\}\right)$.}
\paragraph{\textit{A} = \texttt{v}, ou \texttt{v} est un identifiant de variable}
\[
\begin{aligned}
\overrightarrow{\alpha}(\fwd)(r) 
		&= \alpha\left( \left\{v \in \mathbb{I}_\Omega \setsep \exists \rho \in \dot{\gamma}(r), \deduc{\textit{A}}{\textit{v}} \right\}\right)\\
		&=  \alpha\left( \left\{v \in \mathbb{I}_\Omega \setsep \exists \rho \in \dot{\gamma}(r), \rho({\tutt{v}}) = \textit{v} \right\}\right)\\
		&=  \alpha\left( \left\{ \rho({\tutt{v}}) \setsep \rho \in \dot{\gamma}(r) \right\}\right)\\
		&= \alpha\left( \left\{ \rho({\tutt{v}}) \setsep \rho \in \mathbb{R}, \forall x \in \mathbb{V}, \rho(x) \in \gamma(r(x)) \right\}\right)\\
		&\text{\indent  (d'après la définition de $\dot{\gamma}$, cf \cref{alphaptgammapt})}\\
		&= \alpha \circ \gamma (r({\tutt{v}}))\\
		&\leq r({\tutt{v}})
\end{aligned}
\]

On peut choisir \fbox{${\Xi_\ttt{v} : r \mapsto r({\tutt{v}})}$.} car on ne cherche pas la meilleur approximation.

\paragraph{\textit{A} = \texttt{u} \textit{A'}, ou \texttt{u} est un opérateur unaire}
\[
\begin{aligned}
\overrightarrow{\alpha}(\fwd)(r) 
		&= \alpha\left( \left\{v \in \mathbb{I}_\Omega \setsep \exists \rho \in \dot{\gamma}(r), \deduc{\textit{A}}{\textit{v}} \right\}\right)\\
		&= \alpha\left( \left\{\tutt{u}\ v'\setsep v' \in \mathbb{I}_\Omega, \exists \rho \in \dot{\gamma}(r), \deduc{\textit{A'}}{\textit{v'}} \right\}\right)\\
		&\leq \alpha\left( \left\{\tutt{u}\ v\setsep v \in \gamma \circ \alpha \left\{v' \in \mathbb{I}_\Omega, \exists \rho \in \dot{\gamma}(r), \deduc{\textit{A'}}{\textit{v'}} \right\}\right\}\right)\\	
		&\text{\indent (Car $\gamma \circ \alpha$ est expansive et $\alpha$ croissante.)}\\
		&= \alpha\left( \left\{\tutt{u}\ v\setsep v \in \gamma\left(\overrightarrow{\alpha}\left(\fwd[A']\right)(r)\right)\right\}\right)\\
		&\leq \alpha\left( \left\{\tutt{u}\ v\setsep v \in \gamma\left( {\Xi}_{A'}(r)\right)\right\}\right)\\
		&\text{\indent (Car $\overrightarrow{\alpha}\left(\fwd[A']\right)(r) \leq {\Xi}_{A'}(r)$ et $\alpha$ et $\gamma$ sont croissantes)}
\end{aligned}
\]

On prend donc : \fbox{${\Xi_\text{\texttt{u} \textit{A'}}} : r \mapsto  \alpha\left( \left\{\tutt{u}\ v\setsep v \in \gamma\left( {\Xi}_{A'}(r)\right)\right\}\right)$.}
\pagebreak
\setcounter{tocdepth}{4}
\tableofcontents
\printbibliography
\end{document}