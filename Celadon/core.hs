{-# LANGUAGE MultiParamTypeClasses, FlexibleInstances, UndecidableInstances #-}

data V = A | B | C | D deriving (Show, Eq)

data Calc = Nbr Int |
            Var V |
            Plus Calc Calc | Fois Calc Calc | Div Calc Calc |
            Moins Calc | PPositive Calc | PPNegative

data IOmega = Val Int | ErrInit | ErrArith
max_int =  2^31 - 1
min_int = - 2^31

class TreillisComp a where
  binf :: a -> a -> a
  binf_ens :: [a] -> a

  bsup :: a -> a -> a
  bsup_ens :: [a] -> a

  bot :: a
  top :: a

  binf_ens = foldr binf top
  bsup_ens = foldr bsup bot

  binf a b = binf_ens [a,b]
  bsup a b = bsup_ens [a,b]

  bot = bsup_ens []
  top = binf_ens []

class Fini a where
  liste_exhaustive :: [a]


instance Fini V where liste_exhaustive = [A,B,C,D]

instance Fini IOmega where liste_exhaustive = ErrInit:(ErrArith:(map Val [min_int .. max_int]))

instance (Eq a, Fini a) => TreillisComp [a] where
  bot = []
  top = liste_exhaustive
  bsup l1 l2 = l1 ++ (filter (not . (flip elem l1)) l2)
  binf l1 = filter (flip elem l1)

  
class CorrGal a b where
  abstr :: a -> b
  concr :: b -> a

class PreCorrGall a b where
  preconcr :: b -> a

instance (Eq a, Fini b, TreillisComp a, TreillisComp b, PreCorrGall a b) => CorrGal a b where
  abstr x = binf_ens [y | y <- liste_exhaustive, (binf x (preconcr y)) == x]
  concr = preconcr

data Sign = Bot | Err | Top | Ini | Neg | Zero | Pos

instance TreillisComp Sign where
  top = Top
  bot = Bot

  bsup _ Top = Top
  bsup x Bot = x
  bsup Bot x = x
  bsup Top _ = Top
  bsup Ini Err = Top
  bsup Ini _ = Ini
  bsup Neg Ini = Ini
  bsup Neg Neg = Neg
  bsup Neg _ = Top
  --PAS FINI
  
  binf x Top = x
  binf _ Bot = Bot
  binf Top  x = x
  binf Bot _ = Bot
  binf Ini Err = Bot
  binf Ini x = x
  binf Neg Ini = Neg
  binf Neg Neg = Neg
  binf Neg _ = Bot


  
