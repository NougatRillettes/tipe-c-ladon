module Abstr (
  -- * Treillis Complets
  TreillisComp(..),
  -- * Ensembles utiles
  Prop(..),
  vide,
  appartient,
  ajout,
  Interval(..),
  AbstrErr(..),
  Abstr,
  -- * La correspondance de Galois
  alpha,
  gamma,
  -- * Version abstraite des opérateurs
  aMoins,
  aPNegative,
  aPPositive,
  aPlus,
  aFois,
  aDiv
  )where 

import Nombre
import ABR
import Data.Ratio



-- | Une classe qui représente les treillis complet.
class TreillisComp a where
  -- | Borne inférieure de deux éléments
  binf :: a -> a -> a
  -- | Borne supérieure de deux éléments
  bsup :: a -> a -> a
  -- | Bonre inférieure d'une liste d'éléments
  binf_list :: [a] -> a
  -- | Borne supérieure d'une liste d'éléments  
  bsup_list :: [a] -> a
  -- | Le plus petit élément du treillis
  bot :: a
  -- | Le plus grand éléments du treillis
  top :: a


  binf_list = foldr binf top
  bsup_list = foldr bsup bot

-- | Un produit de treillis reste un treillis
instance (TreillisComp a, TreillisComp b) => TreillisComp (a,b) where
  bot = (bot, bot)
  top = (top, top)

  binf (a,b) (x,y) = (binf a x,binf b y)
  bsup (a,b) (x,y) = (bsup a x,bsup b y)



-- | Pour représenter une propriété (donc un ensemble contenant des nombres et des erreurs), on va la représenter comme une union d'un ensemble de nombres (stocké dans un ABR) et une paire de booléens qui indiquent si chacune des erreurs est présente. On prend un 'ABR' 'Int' 'Int' où les clées seront toutes égales à la valeur.
--
-- On a pas instancié 'TreillisComp' pour 'Prop' car les opérations d'union et d'intersection seraient bien trop long pour être utilisabes en pratique. Par ailleurs, l'analyse menée dans le dossier nous a donné des expressions des différentes fonctions qui n'utilisent plus les opérations ensemblistes sur 'Prop'.
data Prop = Prop { nombres :: (ABR NbrInt NbrInt), contientErrArith :: Bool, contientErrInit :: Bool } deriving Show

-- | 'appartient' sera notre test pour savoir si un nombre appartient à la prop.
appartient :: NbrInt -> Prop -> Bool
appartient x e
 | ((nombres e) ?! x) == Nothing = False
 | otherwise = True

-- | 'ajout' est également du sucre syntaxique
ajout x e = e <-- (x,x)

-- | L'ensemble de nombres vide
vide :: ABR NbrInt NbrInt
vide = End


-- | Des intervalles : on fera le produit avec 'AbstrErr' pour obtenir le domaine abstrait : 'Abstr'
data Interval = Inter (Integer, Integer) | Vide deriving (Eq, Show)

mapBornes f Vide = Vide
mapBornes f (Inter (a,b)) = Inter (f a, f b)

mapInter f Vide = Vide
mapInter f (Inter x) = (Inter (f x))

instance TreillisComp Interval where 
  bot = Vide
  top = Inter (-100, 100)

  binf Vide _ = Vide
  binf _ Vide = Vide
  binf (Inter (a,b)) (Inter (x,y)) = if b >= x && y >= a
                                   then Inter (max a x, min b y)
                                   else Vide

  bsup Vide x = x
  bsup x Vide = x
  bsup (Inter (a,b)) (Inter (x,y)) = Inter (min a x, max b y)

-- | Les abstractions des erreurs, on les réunira ensuite avec 'Interval' pour modéliser les valeurs. On est obligé d'avoir quatre valeurs pour bien in fine obtenir un treillis complet.
data AbstrErr = NErr -- ^ Pas d'erreur
              | ErrArith -- ^ Erreur d'Arith
              | ErrInit -- ^Erreur d'initialisation
              | TErr -- ^ La borne sup. : "les deux erreurs à la fois".
              deriving (Eq, Show)

type Abstr = (AbstrErr, Interval)

instance TreillisComp AbstrErr where
  top = TErr
  bot = NErr

  bsup NErr x = x
  bsup x NErr = x
  bsup ErrArith ErrInit = TErr
  bsup ErrArith ErrArith = ErrArith
  bsup ErrInit ErrArith = TErr
  bsup ErrInit ErrInit = ErrInit
  bsup TErr _ = TErr
  bsup _ TErr = TErr

  binf NErr _ = NErr
  binf _ NErr = NErr
  binf ErrArith ErrInit = NErr
  binf ErrArith ErrArith = ErrArith
  binf ErrInit ErrArith = NErr
  binf ErrInit ErrInit = ErrInit
  binf TErr x = x
  binf x TErr = x



gamma :: Abstr  -> Prop
gamma (e, i) =
  let (err1,err2) = case e of
        ErrArith -> (True, False)
        ErrInit ->  (False, True)
        TErr ->  (True, True)
        NErr -> (False, False)
  in
   let valeurs = case i of
         Vide -> []
         Inter (a,b) -> [(/100).  fromInteger $ x  | x <- [ (100*a ) .. (100*b) ] ]
   in Prop { nombres = foldr ajout End valeurs, -- l'ABR construit va être tout plat donc aucun intérêt, a améliorer.
             contientErrArith = err1,
             contientErrInit = err2
           }
                                          

alpha :: Prop -> Abstr
alpha p =
  let err 
        | (contientErrArith p) && (contientErrInit p) = TErr
        | contientErrArith p = ErrArith
        | contientErrInit p = ErrInit
        | otherwise = NErr
  in
   let a = cleMin . nombres $ p in
   let b = cleMax . nombres $ p in
   let inter = maybe Vide (\x -> Inter (floor x, maybe 0 ceiling b)) a in
   (err, inter)

-- Versions abstraites des opérateurs :

aPPositive (e, i) = (e, mapBornes (max 0) i)

aPNegative (e, i) = (e, mapBornes (min 0) i)

aMoins (e, i) = (e, (mapInter swap) . (mapBornes negate) $ i)
                where swap (a,b) = (b,a)


normaliseInter (a,b)
  | abs a <= 100 && abs b <= 100 = (NErr, Inter (a,b))
  | b < -100 = (ErrArith, Vide)
  | a > 100 = (ErrArith, Vide)
  | b > 100 && abs (a) <= 100 = (ErrArith, Inter (a,100))
  | a < -100 && abs b <= 100 = (ErrArith, Inter (-100,b))
  | otherwise = (ErrArith, Inter (-100, 100))


aPlus (e, i) (f, j) =
  let err = bsup e f in
  case (i,j) of
    (Vide, _) -> (err, Vide)
    (_, Vide) -> (err, Vide)
    (Inter (a,b) , Inter (x,y)) ->
      let (errBornes, inter) = normaliseInter (a + x, b + y)
      in (bsup errBornes err, inter)

aFois (e,i) (f,j) =
  let err = bsup e f in
  case (i,j) of
    (Vide, _) -> (err, Vide)
    (_, Vide) -> (err, Vide)
    (Inter (a,b) , Inter (x,y)) ->
      let inf = minimum [a*x,b*x,a*y,b*y] in
      let sup = maximum [a*x,b*x,a*y,b*y] in
      let (errBornes, inter) = normaliseInter (inf, sup) in
      (bsup errBornes err, inter)

aDiv (e,i) (f,j) =
  let err = bsup e f in
  case (i,j) of
    (Vide, _) -> (err, Vide)
    (_, Vide) -> (err, Vide)
    (Inter (a,b) , Inter (x,y)) ->
      if x * y <= 0
      then (bsup ErrArith err, if x /= y then Inter (-100,100) else Vide)
      else
        let ens = foldr ajout vide (map fromRational [a%x,b%x,a%y,b%y]) in
        let Inter (inf, sup) = snd . alpha $ Prop ens False False in
        let (errBornes, inter) = normaliseInter (inf, sup) in
        (bsup errBornes err, inter)
