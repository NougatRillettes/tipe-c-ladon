-- | Ce module implémente les « nombres avec une précision au centième ». En réalité, on ne fait qu'encapsuler les types d'entiers déjà existat en Haskell afin de les manipuler « comme si ils étaient toujours suivi d'un * 10^-2 ».

module Nombre (Nbr(..), NbrInt) where

import Data.Ratio

-- | 'NbrInt' est isomorphe à 'Integer',
-- le type des entiers de taille arbitraire de
-- Haskell. Il représente les nombres avec une précision
-- au centième et les arrondis se font toujours à la valeur inférieure.
--
-- >>>  1/8 ::Nbr
-- 0.12

type NbrInt = Nbr Integer

{- | Vue la manière dont Haskell gère les typeclasses, on préfère
travailler sur un type paramétré pour
pouvoir l'instancier plus facilement. On le particularise ensuite en
'NbrInt'
 Le fait de prendre un type enregistrement nous permet, grâce à une spécificité d'haskell de disposer de la fonction

 > val :: Nbr a -> a

 avec

prop> val (Nbr x) = x
-}
data Nbr a = Nbr {  val :: a }
           deriving Eq

-- | 'Functor' nous donne 'fmap'
--
-- prop> fmap f (Nbr x) = Nbr (f x)
instance Functor Nbr where
  fmap f = Nbr . f . val


-- | 'Num' donne aux 'Nbr' une structure d'anneau (sans toutesfois garantir qu'il y a des éléments neutres), avec une notion de signe.
instance (Num a, Integral a) => Num (Nbr a) where
  (Nbr a) + (Nbr b) = Nbr ( a + b)
  (Nbr a) - (Nbr b) = Nbr ( a - b)
  (Nbr a) * (Nbr b) = Nbr (quot (a*b) 100)
  negate = fmap negate
  fromInteger = Nbr . (100*) . fromInteger
  abs = fmap abs
  signum = Nbr . signum . val

-- | 'Fractional' dote les nombres d'une multiplication
instance ( Integral a) => Fractional (Nbr a) where
  fromRational = Nbr . truncate . (100*)
  (Nbr a) / (Nbr b) = Nbr (quot (100*a) b)



{- | Instancier 'Real' permet de pouvoir obtenir la valeur d'un
 'Nbr' sous forme de 'Rationnal' (nombre rationnel de précision
 arbitraire représenté en fraction).

>>> toRational (0.2 :: Nbr)
   1 % 5
-}
instance (Integral a, Real a) => Real (Nbr a) where
  toRational = (/ 100) . toRational . val

-- | 'Ord' dote les nombres d'un orde
instance Ord a => Ord (Nbr a) where
         compare x y = compare (val x) (val y)

-- | 'Show' permet d'afficher
instance (Real a) => Show (Nbr a) where
  show = show . fromRational . (/100) . toRational .  val


instance Integral a => RealFrac (Nbr a) where
  properFraction (Nbr x) = let (q,r) = quotRem x 100
                           in (fromIntegral q, Nbr r)
