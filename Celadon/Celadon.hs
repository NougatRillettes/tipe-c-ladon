import Calc
import Abstr

infixl 3 !<!


interprete NaN r = alpha (Prop { nombres = vide , contientErrArith = True, contientErrInit = False })
interprete (Val x) r = if abs x <= 100
                       then alpha (Prop (ajout x vide) False False)
                       else interprete NaN r
interprete (Plus a1 a2) r = aPlus (interprete a1 r) (interprete a2 r)
interprete (Fois a1 a2) r = aFois (interprete a1 r) (interprete a2 r)
interprete (Div a1 a2) r  = aDiv (interprete a1 r) (interprete a2 r)
interprete (PPositive a1) r = aPPositive (interprete a1 r)
interprete (PNegative a1) r = aPNegative (interprete a1 r)
interprete (Moins a1) r = aMoins (interprete a1 r)
interprete (Var x) r = r x


r0 v = (ErrInit, Vide)



(!<!) env (var,a,b) = \v -> if v == var
                          then (NErr, Inter (a,b))
                          else env v

with expr affectation =
  let formule = simpl . calc $ expr in
  let (err, inter) = interprete formule (foldr (flip ( !<!)) r0 affectation) in
  case (err, inter) of
    (NErr,Vide) -> "Il n'y a aucun résultat."
    (ErrArith, Vide) -> listing affectation ++ "l'expression " ++ show formule ++ " s'evalue toujours en une erreur d'arithmetique."
    (ErrInit, _) -> "Une variable n'est pas précisée !"
    (TErr, _) -> ":'("
    (NErr, Inter (a,b)) -> listing affectation ++ "l'expression " ++ show formule ++ " prendra ses valeurs dans ["  ++ show a ++ "," ++ show b ++ "]."
    (ErrArith, Inter (a,b)) -> listing affectation ++ "il pourra y avoir des erreurs, et l'expression " ++ show formule ++ " prendra ses valeurs - lorsqu'elle reussira - dans ["  ++ show a ++ "," ++ show b ++ "]."

listing = foldl (\s (var,a,b) -> if a == b
                                 then s ++ (var:'=':(show a)) ++ ", "
                                 else s ++ ((show a) ++ "<=" ++ [var] ++ "<=" ++ (show b)) ++ ", "
                )
          "Dans les conditions : "
    
