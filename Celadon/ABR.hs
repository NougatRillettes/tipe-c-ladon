-- | Ce module implémente des Arbres de recherche binaire.
module ABR
( -- * Types and Classes
  ABR(End),
  Decidable(decTest),
  (?!),
  (<--),
  (!-),
  cleMin,
  cleMax
) where

import Prelude hiding (foldl, foldr, foldl1, foldr1, mapM_, sequence_,
                elem, notElem, concat, concatMap, and, or, any, all,
                sum, product, maximum, minimum)
import qualified Prelude (foldl, foldr, foldl1, foldr1)
import Data.Foldable

-- | Arbre binaire de recherche dont la clé est de type @k@ est les éléments de type @a@
data ABR k a =  Node k a (ABR k a) (ABR k a) 
             | End -- ^ 'End' représente l'arbre vide. Les arbres doivent être construits avec '<--'   à partir de 'End', et non avec 'Node' (que le module n'exporte d'ailleur pas).
               deriving (Show, Eq)


data Direction = Gauche | Droite deriving (Show, Enum, Eq) 


-- | 'Decidable' donne un test sur les clés de l'arbre pour pouvoir choisir un côté ou l'autre lors de la supression. Ce test doit idéalement être vrai dans 50% des cas et faux sinon afin de garantir des arbres statistiquement équilibrés.
class Decidable k where
    -- | Donne la fonction de décision en elle-même.
    decTest :: k -> Bool

--

-- | Recherche d'une valeur associée à une clé. 'Maybe' est le type option de Haskell
(?!) :: Ord k => ABR k a -> k -> Maybe a                                                      
End ?! _ = Nothing
(Node n x ll rl) ?! k 
    | k == n = Just x 
    | n < k = rl?!k
    | n > k = ll?!k



----

-- | Fonction d'ajout dans un arbre, qui préserve la structure.
(<--) :: Ord k => ABR k a -> (k,a) -> ABR k a -- add
End <-- (n,x) = Node n x End End
(Node n x ll rl) <-- (n',x') = if n < n' then Node n x ll (rl <-- (n',x'))
                               else Node n x (ll <-- (n',x')) rl


-- | Supression d'une clé. Laisse l'arbre inchangé si la clé n'y était pas.
(!-) :: (Ord k, Decidable k) => ABR k a -> k -> ABR k a --deletion
End !- _ = End
(Node n x ll rl) !- k 
    | n < k = Node n x ll (rl !- k)
    | n > k = Node n x (ll !- k) rl
    | n == k =  
        case (ll,rl) of 
          (End, End) -> End
          (End , _ ) -> rl
          ( _ , End) -> ll
          otherwise -> if decTest k
                       then let bar = extractLast rl Gauche id
                            in uncurry Node (snd bar) ll (fst bar)
                       else let bar = extractLast ll Droite id
                            in uncurry Node (snd bar) (fst bar) rl
        
extractLast :: ABR k a -> Direction -> (ABR k a -> ABR k a) -> (ABR k a, (k, a))          
extractLast (Node n x  End rl) Gauche acc = (acc rl , (n,x))
extractLast (Node n x ll End) Droite acc = (acc ll , (n,x))
extractLast (Node n x ll rl) dir acc = case dir of
                                        Gauche -> extractLast ll Gauche $ acc . (\foo -> Node n x foo rl)
                                        Droite -> extractLast rl Droite $ acc . Node n x ll


----

rightRot (Node n x (Node n' x' ll' rl') rl) = Node n' x' ll' (Node n x rl' rl)
rightRot foo = foo

leftRot (Node n x ll (Node n' x' ll' rl')) = Node n' x' (Node n x ll ll') rl'
leftRot foo = foo

------ Classes

instance Functor (ABR k) where
    fmap _ End = End
    fmap f (Node n x ll rl) = Node n (f x) (fmap f ll) (fmap f rl)

instance Foldable (ABR k) where
  foldr f e End = e
  foldr f e (Node _ elt ag ad) = foldr f (foldr f (f elt e) ag) ad

-- Clé min et clé maximum

-- | cleMin renvoie la valeur dont la clé est la plus petite.
cleMin End = Nothing
cleMin (Node _ e End _) = Just e
cleMin (Node _ _ ag _) = cleMin ag

-- | cleMax renvoie la valeur dont la clé est la plus grande
cleMax End = Nothing
cleMax (Node _ e _ End) = Just e
cleMax (Node _ _ _ ad) = cleMax ad
