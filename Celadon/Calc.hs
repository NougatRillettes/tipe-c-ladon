-- | Ce module implémente le langage Calc en lui-même.

module Calc (Calc(..), calc, simpl) where

import Control.Monad.Trans.State.Lazy
import Nombre
import Data.Ratio


-- | 'Calc' est le type des expressions arithmétiques (ie des expression Calc)
data Calc = Val NbrInt -- ^Une valeur numérique (stockée dans un NbrInt)
          | NaN    -- ^Not a number : la valeur d'erreur de Calc
          | Var Char -- ^Tout les identifiants de variables ne pourront faire qu'une lettre
          | Plus Calc Calc
          | Fois Calc Calc
          | Div Calc Calc 
          | Moins Calc
          | PPositive Calc -- ^Partie positive
          | PNegative Calc -- ^ Partie négative
          deriving (Eq)

-- | instancier Show permet de convertir une expression en chaîne de charactères.
-- On parenthèse touts les opérateurs binaires et 'Moins'.
instance Show Calc where
  show (Val x) = show x
  show NaN = "NaN"
  show (Var x) = [x]
  show (Plus a b) = "("++(show a) ++ "+" ++ (show b)++")"
  show (Fois a b) = "("++(show a) ++ "*" ++ (show b)++")"
  show (Div a b) = "("++(show a) ++ "/" ++ (show b)++")"
  show (Moins a) = "(-"++(show a)++")"
  show (PPositive a) = 'P':(show a)
  show (PNegative a) = 'N':(show a)
                   
 -- Parseur

parseNbrInt :: State String NbrInt
parseNbrInt = state (\s ->
                      let (debut,fin) = span ( `elem`  '%':[ '0' .. '9' ])  s in
                      let nbr = if '%' `elem` debut
                                then fromRational $ read debut
                                else fromInteger $ read debut
                      in (nbr, fin))


parseOpBin '+' = Plus
parseOpBin '/' = Div
parseOpBin '*' = Fois

parseOpUn 'P' = PPositive
parseOpUn 'N' = PNegative
parseOpUn '-' = Moins

parse' :: (State String Calc) -> String -> (Calc, String)
parse' f l@(x:xs) 
  | x `elem` ['a' .. 'z'] =
    run xs $
    do return (Var x)
         
  | x `elem` [ '0' .. '9' ] =
    run l $
    do n <- parseNbrInt
       return (Val n)
       
  | x `elem` "+/*" =
      run xs $
      do e1 <- f
         e2 <- f
         return ((parseOpBin x) e1 e2)

  | x `elem` "PN-" =
        run xs $
        do exp <- f
           return ((parseOpUn x) exp)

  | x == ' ' =
          run xs f

  | otherwise =
          run xs $
          do return NaN
           
  where run = flip runState 
parse' _ [] = (NaN,[]) 
            
parser = state $  parse' parser

dansMoinsCentCent :: NbrInt -> Bool
dansMoinsCentCent x = (abs x) <= 100

-- | Calc est la fonction de parsing en elle-même. On rapelle que l'entrée se fait en notation Polonaise inverse. La sortie ne reprend pas le nom des constructeurs car on a instancié 'Show'. Si l'entrée n'est pas correcte, des NaN peuvent apparaître.
--
-- >>> calc "+ 5 * y * P 4 - 6"
-- (5.0+(y*(P4.0*(-6.0))))
--
-- >>> calc "+ + 6 84"
-- ((6.0+84.0)+NaN)
calc = fst . runState parser


-- Simplification d'une expression

simpl' (Plus (Val x) (Val y)) = if dansMoinsCentCent (x + y) then Val (x + y) else NaN
simpl' (Fois (Val x) (Val y)) = if dansMoinsCentCent (x * y) then Val (x * y) else NaN
simpl' (Div  (Val x) (Val y)) = if y /= 0 && dansMoinsCentCent (x / y) then Val (x / y) else NaN
simpl' x = x

-- | 'simpl' simplifie une expression en calculant tout ce qui est possible. Elle sert également à donner des 'NaN' pour des valeurs absolues supérieures à 100. Par contre elle ne propage pas les 'NaN', principalement pour des raisons de debugging.
--
-- >>> simpl . calc $ "+ * + 1 2 y / 1 100"
-- ((3.0*y)+1.0e-2)
simpl (Moins (Val x)) = Val (-x)
simpl (Plus a b) = simpl' (Plus (simpl a) (simpl b))
simpl (Fois a b) = simpl' (Fois (simpl a) (simpl b))
simpl (Div a b) = simpl' (Div (simpl a) (simpl b))
simpl (PPositive a) = simpl' (PPositive (simpl a))
simpl (PNegative a) = simpl' (PNegative(simpl a))
simpl (Moins a) = simpl' (Moins (simpl a))
simpl (Val x) = if abs x <= 100 then Val x else NaN
simpl x = simpl' x
