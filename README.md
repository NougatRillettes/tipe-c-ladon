# Céladon #

Ce repo héberge les fichiers d'un TIPE sur l'interprétation abstraite, notamment le code source de l'implémentation de Céladon : l'exemple du TIPE. L'architecture du dossier est la suivante : 

 * / 
   * fichiers de compilation .tex (.tex lui-même, bibliographie et figure)
   * Céladon
      * fichiers haskell .hs
      * fichiers cabals (assistant de construction de projet haskell)
      * dist 
         * [Documentation]( http://nougatrillettes.bitbucket.org/Celadon/dist/doc/html/Celadon/) du code.

## Comment visualiser le code ?

Pour visualiser le code vous pouvez : 

 * Parcourir le repo (en cliquant sur le bouton "sources" à gauche de l'écran, celui du milieu sur la captur d'écran)

![buttonsource.png](https://bitbucket.org/repo/dgkRGq/images/1404829351-buttonsource.png)

 * Cloner le repo si vous êts un utilisateur de git
 * Télécharger le repo et le parcourir sur votre poste (section Downloads, la dernière)
 * Cliquer sur les liens "source" tout à droite des extraits de code dans la documentation, ils vous amèneront au code coloré (le même que celui hébergé ici).

## Comment est organisé le code ?

Le code s'organise en modules : 

 * **ABR** implémente des abrres binaires de recherche qui servent à gérer les ensembles un peu plus efficacement qu'avec une simple liste
 * **Abstr** implémente les treillis ainsi que les opérateurs sur le domaine abstrait
 * **Nombre** implémente les nombres à la précision au centième de Calc, mais dans une version non bornée.
 * **Calc** implémente le langage Calc en lui-même, notamment le parseur. 
 * **Celadon** implémente l'interface du programme.

Les autres fichiers ne sont pas utilisés par le programme.

## Comment faire tourner le code

Si vous voulez faire tourner le code, vous devez disposer de la plateforme [Haskell](http://www.haskell.org/platform/). Une fois celle-ci lancée, démarrez un invite de commande dans le dossier Celadon et entrez : 


```
#!bash
ghci Celadon.hs
```

puis par exemple (l'entrée se fait en notation préfixe et - est uniquement un opérateur unaire) :

```
#!haskell
*Main> "/ 30 + 4 - x" `with` [('x',2,3)]
```

qui devrait vous renvoyer : 

> Dans les conditions : 2<=x<=3, l'expression (30.0/(4.0+(-x))) prendra ses valeurs dans [15,30].


